#include <iostream>
#include <string>

namespace hello {
   /**
    * Say hi
    * @param name The name
    */
    void say_hi(std::string name);
}
