#include "hello.hpp"

namespace hello {
    void say_hi(std::string name) {
        std::cout << "Hi, " << name << "!" << std::endl;
    }
}
